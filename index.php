<?php

$page = 'home';

$template_path = "templates/"; // trailing slash!
$base_template = $template_path . "base.tpl.php";
$sub_template = $template_path . $page . ".tpl.php";

include $sub_template;

include $base_template;
